package cn.felord.socket.core;

import cn.felord.socket.messaging.Message;

/**
 * The type Abstract web socket messaging template.
 *
 * @param <D> the type parameter
 * @author dax
 * @since 2019 /4/21 16:34
 */
public abstract class AbstractWebSocketMessagingTemplate<D> implements SocketMessageSendingOperations<D> {

    private D defaultDestination;


    @Override
    public void broadcast(D destination, Message<?> message) {
        doBroadcast(destination, message);
    }

    @Override
    public void broadcast(Message<?> message) {
        doBroadcast(this.defaultDestination, message);
    }

    @Override
    public <O> void convertAndBroadcast(D destination, O payload) {
        doBroadcast(destination, payload);
    }

    @Override
    public <O> void convertAndBroadcast(O payload) {
        doBroadcast(defaultDestination, payload);
    }

    @Override
    public void sendToUser(String  eventName, Message<?> message) {
        doSendToUser(eventName, message);
    }


    /**
     * Do broadcast.
     *
     * @param destination the destination
     * @param message     the message
     */
    protected abstract void doBroadcast(D destination, Message<?> message);

    /**
     * Do broadcast.
     *
     * @param <O>         the type parameter
     * @param destination the destination
     * @param payload     the payload
     */
    protected abstract <O> void doBroadcast(D destination, O payload);


    /**
     * Do send to user.
     *
     * @param eventName the event name
     * @param message   the message
     */
    protected abstract void doSendToUser(String  eventName, Message<?> message);


    /**
     * Sets default destination.
     *
     * @param defaultDestination the default destination
     */
    public void setDefaultDestination(D defaultDestination) {
        this.defaultDestination = defaultDestination;
    }

}
