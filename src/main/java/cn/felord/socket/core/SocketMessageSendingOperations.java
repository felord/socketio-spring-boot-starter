package cn.felord.socket.core;

import cn.felord.socket.messaging.Message;

/**
 * The interface Socket message sending operations.
 *
 * @param <D> the type parameter
 * @author Dax
 * @since 17 :42  2019-04-19
 */
public interface SocketMessageSendingOperations<D> {

    /**
     * Send.
     *
     * @param destination the destination
     * @param message     the message
     */
    void broadcast(D destination, Message<?> message);

    /**
     * Send.
     *
     * @param message the message
     */
    void broadcast(Message<?> message);


    /**
     * Convert the given Object to serialized form
     * wrap it as a message and broadcast it to the given destination.
     *
     * @param <O>         the type parameter
     * @param destination the target destination
     * @param payload     the Object to use as payload
     */
    <O> void convertAndBroadcast(D destination, O payload);

    /**
     * Convert and broadcast.
     *
     * @param <O>     the type parameter
     * @param payload the payload
     */
    <O> void convertAndBroadcast(O payload);


    /**
     * Send to user.
     *
     * @param eventName the event name
     * @param message   the message
     */
    void sendToUser(String eventName, Message<?> message);


}
