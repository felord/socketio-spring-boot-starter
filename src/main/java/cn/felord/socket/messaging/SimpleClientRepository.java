package cn.felord.socket.messaging;

import com.corundumstudio.socketio.SocketIOClient;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author dax
 * @since 2019/4/22 23:02
 */
@Slf4j
public class SimpleClientRepository implements ClientRepository {
    private Map<String, SocketIOClient> clients = new ConcurrentHashMap<>();

    @Override
    public void add(String key, SocketIOClient client) {
        log.info("client {} is online now ", key);
        clients.putIfAbsent(key, client);
    }

    @Override
    public SocketIOClient get(String key) {
        return clients.get(key);
    }

    @Override
    public void remove(String key) {
        log.info("client {} is offline now ", key);
        clients.remove(key);
    }

    @Override
    public void removeAll() {
        clients.clear();
    }
}
