package cn.felord.socket.messaging;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * The type Simple message.
 *
 * @param <T> the type parameter
 * @author dax
 * @since 2019 /4/21 16:35
 */
@Data
public class SimpleMessage<T> implements Message<T> {

    private static final long serialVersionUID = 6934264529216760293L;
    private T payload;
    private String from = "<未命名>";
    private String to = "<未命名>";
    private long timestamp = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
}
