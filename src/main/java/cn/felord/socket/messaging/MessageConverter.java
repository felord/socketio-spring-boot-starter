package cn.felord.socket.messaging;

/**
 * The interface Message converter.
 *
 * @author Dax
 * @since 20 :34  2019-04-20
 */
public interface MessageConverter<T> {

    /**
     * From message object.
     *
     * @param message     the message
     * @param targetClass the target class
     * @return the object
     */
    T fromMessage(Message<T>message,Class<T> targetClass);

    /**
     * To message message.
     *
     * @param payload the payload
     * @return the message
     */
    Message<T> toMessage(T payload);
}
