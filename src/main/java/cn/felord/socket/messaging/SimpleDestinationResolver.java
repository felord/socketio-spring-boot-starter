package cn.felord.socket.messaging;

import org.springframework.util.AntPathMatcher;

/**
 * @author dax
 * @since 2019/4/21 19:44
 */
public class SimpleDestinationResolver implements DestinationResolver<String> {
    private static final String PATTERN = "/**/**";
    private static final int LENGTH = 3;

    @Override
    public Symbol resolve(String destination) {
        String[] split = split(destination);

        String namespace = split[1];
        String eventName = split[2];
        return new Symbol(namespace, eventName);

    }


    private String[] split(String destination) {

        AntPathMatcher antPathMatcher = new AntPathMatcher();

        if (antPathMatcher.match(PATTERN, destination)) {
            String[] split = destination.split("/");
            if (LENGTH == split.length) {
                return split;
            }
        }
        throw new IllegalArgumentException("destination must match /**/**");
    }
}
