package cn.felord.socket.messaging;

/**
 * The type Simple message converter.
 *
 * @author dax
 * @since 2019 /4/21 16:47
 */
public class SimpleMessageConverter<T> implements MessageConverter<T> {


    @Override
    public T fromMessage(Message<T> message, Class<T> targetClass) {
        return message.getPayload();
    }

    @Override
    public Message<T> toMessage(T payload) {
        SimpleMessage<T> objectSimpleMessage = new SimpleMessage<>();
        objectSimpleMessage.setPayload(payload);
        return objectSimpleMessage;
    }
}
