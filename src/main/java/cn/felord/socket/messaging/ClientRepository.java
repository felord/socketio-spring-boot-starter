package cn.felord.socket.messaging;

import com.corundumstudio.socketio.SocketIOClient;

/**
 * The interface Client repository.
 *
 * @author dax
 * @since 2019 /4/22 22:55
 */
public interface ClientRepository {

    /**
     * Add.
     *
     * @param key    the key
     * @param client the client
     */
    void add(String key, SocketIOClient client);

    /**
     * Get socket io client.
     *
     * @param key the key
     * @return the socket io client
     */
    SocketIOClient get(String key);

    /**
     * Remove.
     *
     * @param key the key
     */
    void remove(String key);


    /**
     * Remove all.
     */
    void removeAll();

}
