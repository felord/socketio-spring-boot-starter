package cn.felord.socket.messaging;

/**
 * The interface Destination resolver.
 *
 * @param <D> the type parameter
 * @author dax
 * @since 2019 /4/21 19:42
 */
public interface DestinationResolver<D> {


    /**
     * Resolve symbol.
     *
     * @param destination the destination
     * @return the symbol
     */
    Symbol resolve(D destination);


}
