package cn.felord.socket.messaging;

import com.corundumstudio.socketio.SocketIONamespace;
import org.springframework.beans.factory.InitializingBean;

import java.util.Optional;

/**
 * 名称空间注册.
 *
 * @author Dax
 * @since 09 :40  2019-05-17
 */

public interface NameSpaceWrapper extends InitializingBean {

    /**
     * Namespace socket io namespace.
     *
     * @return the socket io namespace
     */
    SocketIONamespace namespace();

    /**
     * Listeners socket io namespace.
     *
     * @return the socket io namespace
     */
     Object annotationListeners() ;


    /**
     *
     *  配置监听注解类
     *
     * @throws Exception ex
     */
    @Override
    default void afterPropertiesSet() throws Exception {
        Optional<SocketIONamespace> optional= Optional.of(namespace());
        if (annotationListeners() != null) {
            SocketIONamespace socketIONamespace = optional.get();
            socketIONamespace.addListeners(annotationListeners());
        }
    }
}
