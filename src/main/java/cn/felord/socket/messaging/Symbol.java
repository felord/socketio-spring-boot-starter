package cn.felord.socket.messaging;

/**
 * The type Symbol.
 *
 * @author dax
 * @since 2019 /4/21 20:52
 */
public class Symbol {

    private String namespace;
    private String eventName;

    public Symbol(String namespace, String eventName) {
        this.namespace = namespace;
        this.eventName = eventName;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getEventName() {
        return eventName;
    }
}
