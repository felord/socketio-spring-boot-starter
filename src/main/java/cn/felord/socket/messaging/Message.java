package cn.felord.socket.messaging;

import java.io.Serializable;

/**
 * The interface Message.
 *
 * @param <T> the type parameter
 * @author Dax
 * @since 17 :44  2019-04-19
 */
public interface Message<T> extends Serializable {

    /**
     * Gets payload.
     *
     * @return the payload
     */
    T getPayload();

    /**
     * From string.
     *
     * @return the string
     */
    String getFrom();

    /**
     * To string.
     *
     * @return the string
     */
    String getTo();

    /**
     * Gets msg timestamp.
     *
     * @return the timestamp
     */
    long getTimestamp();

}
