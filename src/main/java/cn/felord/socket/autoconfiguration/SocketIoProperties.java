package cn.felord.socket.autoconfiguration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * #============================================================================
 * # netty socket io setting
 * #============================================================================
 * # host在本地测试可以设置为localhost或者本机IP，在Linux服务器跑可换成服务器IP
 * socketio.host=localhost
 * socketio.port=9099
 * # 设置最大每帧处理数据的长度，防止他人利用大数据来攻击服务器
 * socketio.maxFramePayloadLength=1048576
 * # 设置http交互最大内容长度
 * socketio.maxHttpContentLength=1048576
 * # socket连接数大小（如只监听一个端口boss线程组为1即可）
 * socketio.bossCount=1
 * socketio.workerThreads=100
 * socketio.allowCustomRequests=true
 * # 协议升级超时时间（毫秒），默认10秒。HTTP握手升级为ws协议超时时间
 * socketio.upgradeTimeout=1000000
 * # Ping消息超时时间（毫秒），默认60秒，这个时间间隔内没有接收到心跳消息就会发送超时事件
 * socketio.pingTimeout=6000000
 * # Ping消息间隔（毫秒），默认25秒。客户端向服务器发送一条心跳消息间隔
 * socketio.pingInterval=25000
 *
 * @author dax
 * @since 2019 /4/19 0:04
 */
@Data
@ConfigurationProperties(prefix = "socketio")
public class SocketIoProperties {
    /**
     * the server host  default localhost
     */
    private String host = "localhost";
    /**
     * the server port  default 8080
     */
    private int port = 8080;
    /**
     * the max frame payload length  default value 1048576
     */
    private int maxFramePayloadLength = 1048576;
    /**
     * the max http content length default value 1048576
     */
    private int maxHttpContentLength = 1048576;
    /**
     * socket连接数大小（如只监听一个端口boss线程组为1即可）
     */
    private int bossCount = 1;
    /**
     * worker threads  default 100
     */
    private int workerThreads = 100;
    /**
     * weather or not allow custom requests default  true
     */
    private boolean allowCustomRequests = true;
    /**
     * the protocol upgrade time out  default 10s
     */
    private int upgradeTimeout = 10000;
    /**
     * the ping upgrade time out   default 1min
     */
    private int pingTimeout = 60000;
    /**
     * the ping  interval  default 25s
     */
    private int pingInterval = 25000;
    /**
     * 用户客户端标示 用于点对点 default username
     */
    private String clientKey="username";

}
