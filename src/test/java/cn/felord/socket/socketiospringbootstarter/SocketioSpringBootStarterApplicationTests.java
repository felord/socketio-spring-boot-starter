package cn.felord.socket.socketiospringbootstarter;

import cn.felord.socket.autoconfiguration.SocketIoProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SocketioSpringBootStarterApplicationTests {
    @Resource
    SocketIoProperties socketIoProperties;

    @Test
    public void contextLoads() {

        System.out.println("socketIoProperties.getHost() = " + socketIoProperties.getHost());
        int port = socketIoProperties.getPort();
        System.out.println("port = " + port);
    }

}
